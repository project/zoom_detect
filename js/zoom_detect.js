(function (Drupal, $) {
  'use strict';

  Drupal.behaviors.zoom_detect = {
    zoomDetect: function () {
      let lastPx_ratio = 1;
      let px_ratio = 1;
      let newPx_ratio = 0;
      let zoom = "zoom-no";
      let scrollBarWidth = 0;

      function init() {
        getScrollbarWidth();
        checkZoom();
      }

      function isFireFox() {
        const userBrowser = (navigator.userAgent).toLowerCase().indexOf('firefox');
        if (userBrowser > -1) {
          return true;
        }
        return false;
      }

      function isIE() {
        const userBrowseriE = (navigator.userAgent).toLowerCase().indexOf('msie');
        const userBrowseriE11 = (navigator.userAgent).toLowerCase().indexOf('rv:11');
        if (userBrowseriE > -1 || userBrowseriE11 > -1) {
          return true;
        }
        return false;
      }

      function getScrollbarWidth() {
       let initialState = document.getElementsByTagName("html")[0].style.overflow;
       document.getElementsByTagName("html")[0].style.overflow = 'hidden';
       let screen = document.documentElement.clientWidth;
       document.getElementsByTagName("html")[0].style.overflow = 'scroll';
       let screenBar = document.documentElement.clientWidth;
       document.getElementsByTagName("html")[0].style.overflow = initialState;
       scrollBarWidth = screen - screenBar;
      }

      function round5(x) {
        return x % 5 < 3 ? (x % 5 === 0 ? x : Math.floor( x / 5 ) * 5) : Math.ceil( x / 5 ) * 5;
      }

      function isZooming() {
        newPx_ratio = round5((window.screen.availWidth / (document.documentElement.clientWidth + scrollBarWidth)) * (window.outerWidth / screen.width) * 100);
        if(isFireFox() || isIE()) {
          newPx_ratio = round5(window.devicePixelRatio * 100);
        }
        if(newPx_ratio != px_ratio) {
          lastPx_ratio = px_ratio;
          px_ratio = newPx_ratio;
          return true;
        }
      }

      window.addEventListener('resize', function(event){
        checkZoom();
      });

      function ZoomInOrOut() {
        zoom=' zoom-out';
        if(newPx_ratio > lastPx_ratio) {
          zoom=' zoom-in';
        }
        if(newPx_ratio == lastPx_ratio) {
          zoom=' zoom-no';
        }
      }

      function checkZoom() {
        if(isZooming()) {
          removeClass();
          ZoomInOrOut();
          addClass();
        }
      }

      function removeClass() {
        let tagHTML = document.querySelectorAll("html")[0];
        tagHTML.className = tagHTML.className.replace(/ zoom-(in|out|[0-9]*)/g, "");
      }

      function addClass() {
        document.getElementsByTagName('html')[0].className += zoom + " zoom-" + Math.round(px_ratio);
      }

      init();
    },

    attach: function (context) {
      const hasHtmlTag = $('html', context).length;
      if (hasHtmlTag) {
        Drupal.behaviors.zoom_detect.zoomDetect();
      }
    }
  };

})(Drupal, jQuery);
