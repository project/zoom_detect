CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This project allows you to identify the browser and the device used. A class
will be added to the HTML tag.


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


CONFIGURATION
-------------

 * There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Rafael Nica (rafael.nica) - https://www.drupal.org/user/1067692
